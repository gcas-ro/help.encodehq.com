## **GET** /, **GET** /help
Both of these redirect to [help.encodehq.com](https://help.encodehq.com) containing the HTML documentation for the API.

## **GET** /status
This endpoint is an easily accessible way of querying the health state of the EncodeHQ service. In short, a healthy response yields `200 OK` as the HTTP status code and presents a short JSON structure similar to:

```javascript
{
    /**
     * Statistic counters.
     */
    "states": {
        "queued": 12,
        "running": 193,
        "complete": 30210,
        "failed": 1992
    },

    /**
     * API version.
     */
    "version": "1.0.1",

    /**
     * UTC ISO timestamp for the generated status.
     */
    "timestamp": "2017-03-06T18:21:38.356Z"
}
```

---
&copy; Cloud Architectural Inc.
